<?php 

namespace controllers;

abstract class AbstractController
{   
    protected $request;

    public function __construct( $request )
    {
        $this->request = $request;
    }

    public abstract function view();

    public function test()
    {
        print '</br> THIS IS TEST';
    }
}

