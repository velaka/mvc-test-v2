<?php

namespace controllers;

use models\Student;
use models\StudentAbout;
use libs\Request;

class StudentController extends AbstractController
{

    public function view()
    {
        print 'STUDENTS PAGE';
    }
    
    public function getstudent()
    {
        $params = $this->request->getParams();

        $studentInfo = new Student();

        $info = $studentInfo->getStudent( $params['name'] );
        // print_r( implode( ' ',  $info ) );

        print_r(  $info );

    }

    public function checkage()
    {
        $params = $this->request->getParams();

        $studentAgeInfo = new Student();

        $age = $studentAgeInfo->checkStudentAge( $params['from'] , $params['to'] );
        
        print_r( $age );
        
    }

    public function studentuni()
    {
        $params = $this->request->getParams();

        $studentUni = new Student();

        $uni = $studentUni->checkUni( $params['uni'] );
        
        print_r( $uni );
    }


    public function about()
    {
        $allStudentsList = new StudentAbout();

        $allStudentsList->all();
    }

}