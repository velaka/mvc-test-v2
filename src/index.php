<?php

spl_autoload_register( function( $className )
{
    
    $className = str_replace('\\', '/', $className);

    require_once  $className . '.php';
});

use controllers\AbstractController;
use controllers\HomeController;
use controllers\AboutController;
use controllers\ContactController;
use controllers\StudentController;
use libs\Request;
use libs\Router;

$request = new Request();

$router = new Router( $request );

$newController  = $router->getController();
$newAction      = $router->getAction();
$testArray      = array($newAction);


switch ( $newController ) {
    case 'home':
        $newPage = new HomeController( $request );

        if ( testActionExist( $newPage, $newAction ) ) {
            $newPage->$newAction();
        } else {
            echo 'Wrong';
        }            
                   
        break;

    case 'student':
        $newPage = new StudentController( $request );

        if ( testActionExist( $newPage, $newAction ) ) {
            $newPage->$newAction();
        } else {
            echo 'Wrong';
        } 

        break;
    
    case 'about':
        $newPage = new AboutController( $request );
        
        if ( testActionExist( $newPage, $newAction ) ) {
            $newPage->$newAction();
        } else {
            echo 'Wrong';
        }     

        break;
    
    case 'contact':
        $newPage = new ContactController( $request );
        
        if ( testActionExist( $newPage, $newAction ) ) {
            $newPage->$newAction();
        } else {
            echo 'Wrong';
        }     

        break;
    
    default:
        $newPage = new HomeController( $request );
        $newPage->view();
        break;
}


function testActionExist( $page, $action )
{
    return method_exists( $page, $action );
}

?>