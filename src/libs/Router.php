<?php

namespace libs;

class Router
{
    private $request;

    protected $controller;

    protected $action;

    /*
     * @brif Router constructor
     * 
     * @param string $uri
     * 
     * @return void
     */

     public function __construct( $request ) 
     {
        $this->request = $request;
        $this->parse();
     }

     /*
     * @brif    Parse uri string to controller and action
     * 
     * @return void
     */

     protected function parse()
     {
         $parseResult   = parse_url( $this->request->getURI() );
         $explodeResult = explode( '/', trim( $parseResult["path"], '/' ) );

         $this->controller  = $explodeResult[0];

         if ( isset( $explodeResult[1] ) )
         {
            $this->action       = $explodeResult[1];
         } else {
            $this->action       = 'view';
         }
     }

     /*
     * @brif    Return controller based on uri
     * 
     * @return string
     */

    public function getController()
    {
        return $this->controller;
    }

    /*
    * @brif Return action for controller based on uri
    * 
    * @return string
    */

    public function getAction()
    {
        return $this->action;
    }

}