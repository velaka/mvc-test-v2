<?php

namespace libs;

class Request
{

    public function getURI()
    {
        return $_SERVER['REQUEST_URI'];
    }

    public function getQueryParams()
    {
        return $_GET;
    }

    public function getPostParams()
    {
        return $_POST;
    }
    
    public function getParams()
    {
        return $_REQUEST;
    }
}