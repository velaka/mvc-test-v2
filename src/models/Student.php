<?php

namespace models;

class Student
{ 
    protected $studentInfo = array(
        array(
            'name'      => 'velko',
            'age'       => '2',
            'uni'       => 'ty' 
        ),

        array(
            'name'      => 'sinan',
            'age'       => '20',
            'uni'       => 'pu' 
        ),
        
        array(
            'name'      => 'ivan',
            'age'       => '10',
            'uni'       => 'su' 
        ) ,

        array(
            'name'      => 'bojo',
            'age'       => '33',
            'uni'       => 'pu' 
        ),

        
        array(
            'name'      => 'ico',
            'age'       => '21',
            'uni'       => 'su' 
        ) 

    );


    public function getStudent( $name )
    {
        foreach ( $this->studentInfo as $student )
        {
            if( $student['name'] == $name ) {
                return $student;
            } 
        }

    }

    public function checkStudentAge( $from, $to ) 
    {
        $studentAgeArray = array();

        foreach ($this->studentInfo as $studentAge) 
        {
            if ( $studentAge['age'] >= $from && $studentAge['age'] <= $to ) {
                array_push( $studentAgeArray, $studentAge );
            }    
        }


        return $studentAgeArray;

    }

    public function checkUni( $uni )
    {
        $studentUni = array();

        foreach ( $this->studentInfo as $student )
        {

            if ( $student['uni'] == $uni )
            {
                array_push( $studentUni, $student );
            }

        }

        return $studentUni;
     }


}